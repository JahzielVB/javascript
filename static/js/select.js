$(document).ready(function() {
  // Console print
  console.log("Funcionando");

  // Select by element
  $("#button1").click(function() {
    $("h1").html("Select by element h1");
  });

  // Select by class
  $("#button2").click(function() {
    $(".text-center").html("Select by class .");
  });

  // Select by ID
  $("#button3").click(function() {
    $("#idh1").html("Select by ID #");
  });
});
