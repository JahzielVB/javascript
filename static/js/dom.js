$(document).ready(function() {
  // Console print
  console.log("Funcionando");

  $("#button1").click(function() {
    $("#idh1").addClass("text-danger");
  });

  $("#button2").click(function() {
    $("#idh1").removeClass("text-danger");
  });

  $("#button3").click(function() {
    $(".headerText").append("<h1>Add this h1 to the end</h1>");
  });

  $("#button4").click(function() {
    $(".headerText").prepend("<h1>Add this h1 to the start</h1>");
  });

  $("#button5").click(function() {
    $("#idh1").toggleClass("text-uppercase");
  });

  $("#button6").click(function() {
    $("#idh1").css({ color: "#fff", background: "salmon", padding: "20px" });
  });

  $("#button7").click(function() {
    $("img").attr(
      "src",
      "https://a.thumbs.redditmedia.com/zDOFJTXd6fmlD58VDGypiV94Leflz11woxmgbGY6p_4.png"
    );
  });
});
