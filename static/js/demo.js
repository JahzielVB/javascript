$(document).ready(function() {
  // Console print
  console.log("Demo works!!");

  $("#buttonSubmit").click(function(e) {
    e.preventDefault();
    if ($("form")[0].checkValidity() === true) {
      $("form").addClass("was-validated");
      $.ajax({
        url: "/TestAJAX",
        data: $("form").serialize(),
        type: "POST",
        success: function(response) {
          var obj = JSON.parse(response);
          $("#alertMes").html(obj.status);
          $(".alert").show();
          console.log(response);
        },
        error: function(response) {
          console.log(response);
        }
      });
    }
  });
});
