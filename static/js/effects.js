$(document).ready(function() {
  // Console print
  console.log("Funcionando");

  $("#button1").click(function() {
    $("#idh1").toggle(500);
  });

  $("#button2").click(function() {
    $("#idh1").fadeToggle(500);
  });

  $("#button3").click(function() {
    $("#idh1").slideToggle("slow");
  });

  $("#button4").click(function() {
    $("#idh1").animate({
      left: "250px",
      height: "150px",
      width: "150px",
      color: "blue"
    });
  });

  $("#button5").click(function() {
    $("#idh1")
      .slideUp(2000)
      .slideDown(2000);
  });
});
