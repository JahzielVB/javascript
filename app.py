from flask import Flask, render_template, request, json, redirect
from flaskext.mysql import MySQL
from werkzeug.security import generate_password_hash, check_password_hash

app = Flask(__name__)
mysql = MySQL()
app.secret_key = 'mysecretkey'

app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = 'root'
app.config['MYSQL_DATABASE_DB'] = 'BucketList'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
mysql.init_app(app)


@app.route('/')
def hello():
    return render_template('index.html')


@app.route('/select')
def select():
    return render_template('select.html')


@app.route('/dom')
def dom():
    return render_template('dom.html')


@app.route('/events')
def events():
    return render_template('events.html')


@app.route('/effects')
def effects():
    return render_template('effects.html')


@app.route('/demo')
def demo():
    return render_template('demoSignUp.html')


@app.route('/signin')
def sigin():
    return render_template('demoSignIn.html')


@app.route('/userHome')
def userHome():
    return render_template('userHome.html')


@app.route('/vadilateSignIn', methods=['POST'])
def vadilateSignIn():
    try:
        email = request.form['inputEmail']
        password = request.form['up']

        conn = mysql.connect()
        cursor = conn.cursor()

        cursor.callproc('sp_validateLogin', (email,))
        data = cursor.fetchall()

        if len(data) > 0:
            if check_password_hash(str(data[0][3]), password):
                return redirect('/userHome')
            else:
                return json.dumps({'status': 'Wrong Email address or Password.'})
        else:
            return json.dumps({'status': 'Wrong Email address or Password.'})
    except Exception as e:
        return json.dumps({'error': e})
    finally:
        cursor.close()
        conn.close()


@app.route('/TestAJAX', methods=['POST'])
def TestAJAX():
    try:
        name = request.form['inputName']
        email = request.form['inputEmail']
        password = request.form['up2']

        conn = mysql.connect()
        cursor = conn.cursor()

        _hashed_password = generate_password_hash(password)
        cursor.callproc('sp_createUser', (name, email, _hashed_password))
        data = cursor.fetchall()

        if len(data) is 0:
            conn.commit()
            return json.dumps({'status': 'User created successfully !'})
        else:
            return json.dumps({'status': str(data[0])})
    except Exception as e:
        return json.dumps({'error': e})
    finally:
        cursor.close()
        conn.close()


if __name__ == '__main__':
    app.run(debug=True)
